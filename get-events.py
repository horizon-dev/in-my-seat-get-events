import re
import xml.etree.ElementTree as ET

import lxml.html as html

import requests


def get_nottingham_events():
    url = 'https://www.nottingham.ac.uk/events/events.aspx?Calendar_List_EventDateSpan=All&Calendar_List_SyndicationType=1'
    r = requests.get(url)
    root = ET.fromstring(r.text)
    namespaces = {
        'media': 'http://search.yahoo.com/mrss/',
        'xhtml': 'http://www.w3.org/1999/xhtml',
    }
    events = []
    for e in root[0].findall('item'):
        try:
            link_url = e.find('link').text
            details = {
                'title': e.find('title').text,
                'link': link_url,
            }
            thumbnail = e.find('media:thumbnail', namespaces)
            if thumbnail:
                details['thumbnail'] = thumbnail
            r = requests.get(link_url)
            doc = html.fromstring(r.text)
            details['date'] = doc.xpath('.//*[@id="content"]//dd[contains(@class, "sys_events-date")]')[0].text
            details['location'] = doc.xpath('.//*[@id="content"]//dd[contains(@class, "sys_events-location")]')[0].text
            desc = doc.xpath('.//*[@id="content"]//dd[contains(@class, "sys_events-description")]')[0]
            details['description'] = html.tostring(desc, encoding='utf-8').decode('utf-8').strip()
            events.append(details)
        except:
            pass
    return events


def get_nottingham_su_events():
    url = 'https://www.su.nottingham.ac.uk/events/'
    base = 'https://www.su.nottingham.ac.uk'
    r = requests.get(url)
    doc = html.fromstring(r.text)
    event_nodes = doc.xpath('.//div[contains(@class, "eventlist_day")]')
    events = []
    for en in event_nodes:
        for item in en.xpath('.//div[contains(@class, "event_item")]'):
            details = {}
            details['title'] = item.xpath('.//a[contains(@class, "msl_event_name")]')[0].text
            details['thumbnail'] = base + item.xpath('.//span[contains(@class, "msl_event_image")]//img')[0].get('src')
            details['date'] = item.xpath('.//dd[contains(@class, "msl_event_time")]')[0].text
            details['location'] = item.xpath('.//dd[contains(@class, "msl_event_location")]')[0].text
            details['description'] = item.xpath('.//dd[contains(@class, "msl_event_description")]')[0].text
            details['link'] = base + item.xpath('.//a[contains(@class, "msl_event_name")]')[0].get('href')
            events.append(details)
    return events

def get_wollaton_hall_events():
    eurl = 'https://www.wollatonhall.org.uk/events/?date=2019-0{}-01'
    events = []
    base = 'https://www.wollatonhall.org.uk'
    pattern = re.compile(r'\s+')
    for i in range(5,8):
        url = eurl.format(i)
        r = requests.get(url)
        doc = html.fromstring(r.text)
        event_nodes = doc.xpath('.//div[contains(@class, "events-results")]//div[contains(@class, "single-event") and contains(@class, "wollatonhall")]')
        for en in event_nodes:
            details = {}
            details['title'] = en.xpath('.//h2/span')[0].text
            details['thumbnail'] = base + en.xpath('.//div[contains(@class, "img")]/img')[0].get('src')
            details['date'] = pattern.sub(' ', en.xpath('.//span[contains(@class, "date")]')[0].text_content().strip())
            details['location'] = 'Wollaton Hall'
            desc = doc.xpath('.//div[contains(@class, "details")]')[0]
            details['description'] = pattern.sub(' ', html.tostring(desc, encoding='utf-8').decode('utf-8').strip())
            details['link'] = base + en.xpath('./a')[0].get('href')
            events.append(details)
    return events

events = []
events.extend(get_nottingham_events())
get_nottingham_su_events()
get_wollaton_hall_events()
"""

    <Placemark>
      <name>Bus Stop: George Green Library</name>
      <Point>
        <coordinates>
          -1.1923704,52.9411713,0
        </coordinates>
      </Point>
    </Placemark>
"""
kml = ET.Element('kml', { 'xmlns': 'http://www.opengis.net/kml/2.2' })
doc = ET.SubElement(kml, 'Document')
name = ET.SubElement(doc, 'name')
name.text = 'Events'
for event in events:
    pm = ET.SubElement(doc, 'Placemark')
    name = ET.SubElement(pm, 'name')
    name.text = event['title']
    desc = ET.SubElement(pm, 'description')
    desc.text = ''
    if 'thumbnail' in event:
        desc.text = '<img src="{}" />'.format(event['thumbnail'])
    desc.text = desc.text + '<br />{}<br />Date: {}<br />Further details: <a href="{}">{}</a><br />Events'.format(event['description'], event['date'], event['link'], event['link'])
    point = ET.SubElement(pm, 'Point')
    coords = ET.SubElement(point, 'coordinates')
    coords.text = '-1.1992268,52.9423612,0'



ET.dump(kml)
